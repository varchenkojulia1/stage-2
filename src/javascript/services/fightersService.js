import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;

    const endpoint = `details/fighter/${id}.json`;
    const fighter = await callApi(endpoint, 'GET')
      .then(response => response);

    return fighter
  }
}

export const fighterService = new FighterService();
