import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    let first = {...firstFighter};
    let second = {...secondFighter};

        window.addEventListener('keydown', (e) => {
          let action = [];
          let keys = Object.keys(controls);
          keys.forEach(key => {
            if (controls[key] === e.code) {
              action.push(key)
            }
          });
          let damage;
          switch (action){
            case 'PlayerOneAttack':
              damage = getDamage(first, second);
              second.health -= Math.round(damage) ;
              break
            case 'PlayerTwoBlock':
              second.health
            case 'PlayerOneBlock' || 'PlayerTwoAttack':
              damage = getDamage(secondFighter, firstFighter);
              firstFighter.health -= Math.round(damage);
              break
            }}
          )



    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  // return damage
  return getHitPower(attacker) - getBlockPower(defender) ? getHitPower(attacker) - getBlockPower(defender) : 0;
}
export function getHitPower(fighter) {
  let criticalHitChance = Math.random()*(2-1) + 1;
  let hitPower = fighter.attack*criticalHitChance;
  // return hit power
  return hitPower
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random()*(2-1) + 1;
  let power = fighter.defense * dodgeChance
  // return block power
  return power
}
