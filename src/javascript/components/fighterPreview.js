import { createElement } from '../helpers/domHelper';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
    const fighterName = createElement({
      tagName: 'p',
      className: `fighter-preview___name`
    });
    fighterName.innerHTML = fighter[1].name;
    fighterElement.appendChild(fighterName);


  let img = createFighterImage({ name: fighter[1].name,
    source: fighter[1].source
  });
  fighterElement.appendChild(img);

  const fighterHealth = createElement({
    tagName: 'p',
    className: `fighter-preview___health`
  });
  fighterHealth.innerHTML = fighter[1].health;
  fighterElement.appendChild(fighterHealth);

  const fighterAttack = createElement({
    tagName: 'p',
    className: `fighter-preview___attack`
  });
  fighterAttack.innerHTML = fighter[1].attack;
  fighterElement.appendChild(fighterAttack);

  const fighterDefense = createElement({
    tagName: 'p',
    className: `fighter-preview___defense`
  });
  fighterDefense.innerHTML = fighter[1].defense;
  fighterElement.appendChild(fighterDefense);


  return fighterElement
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}